package info.test.myCiGitLabTestProgect;

import junit.framework.TestCase;
import org.junit.Test;

public class CalculatorTest extends TestCase {
    Calculator calculator = new Calculator();

    @Test
    public void sum() {
        int actual = 25;
        assertEquals(calculator.sum(10, 15), actual);
    }

    @Test
    public void multiplication() {
        int actual = 30;
        assertEquals(calculator.multiplication(5, 6), actual);
    }
}